package com.qd.model;

import java.io.Serializable;

public class InfoWindow implements Serializable {

    private String imgResto;
    private String textResto;
    private int position;

    public String getImgResto() {
        return imgResto;
    }

    public void setImgResto(String imgResto) {
        this.imgResto = imgResto;
    }

    public String getTextResto() {
        return textResto;
    }

    public void setTextResto(String textResto) {
        this.textResto = textResto;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
