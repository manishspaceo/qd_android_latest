package com.qd.interfaces;

public interface OnTaskCompleteListener {

	void onComplete(String resultString);

}
