package com.qd.gcm;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.qd.R;
import com.qd.SplashActivity;
import com.qd.util.ApplicationQd;
import com.qd.util.QdPreferences;

import java.util.List;
import java.util.Random;

/**
 * Created by sotsys171 on 23/6/15.
 */
public class AppGcmListenerService extends GcmListenerService {

    private static final String TAG = "AppGcmListenerService";
    private int NOTIFICATION_ID;
    private boolean flag;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.e("TAG", "" + data);

        String message = data.getString("message");
        NOTIFICATION_ID = new Random().nextInt(9999 - 1000) + 1000;


        QdPreferences qdPreferences = ApplicationQd.getInstance().getSharedPreferences();

        Log.d(TAG, "NOTIFICATION_ID = " + NOTIFICATION_ID);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setAutoCancel(true)
                .setContentText(message)
                .setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_LIGHTS)
                .setVibrate(new long[]{100, 200});

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            mBuilder.setColor(ContextCompat.getColor(this, R.color.color_primary));
            mBuilder.setSmallIcon(R.mipmap.ic_launcher);

        } else {

            mBuilder.setSmallIcon(R.mipmap.ic_launcher);

        }

        PendingIntent pendingIntent = null;
        Intent intent = new Intent(this, SplashActivity.class);
        pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        mBuilder.setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, mBuilder.build());

    }
    // [END receive_message]
}