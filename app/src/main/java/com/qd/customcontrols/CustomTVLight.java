package com.qd.customcontrols;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTVLight extends TextView {

    public CustomTVLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTVLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTVLight(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "HindVadodara-Light.ttf");
        setTypeface(typeface, 1);

    }

}