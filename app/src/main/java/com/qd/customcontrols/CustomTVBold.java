package com.qd.customcontrols;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTVBold extends TextView {

    public CustomTVBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTVBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTVBold(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "HindVadodara-Bold.ttf");
        setTypeface(typeface, 1);

    }

}