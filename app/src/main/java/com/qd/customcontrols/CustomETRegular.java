package com.qd.customcontrols;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class CustomETRegular extends EditText {

    public CustomETRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomETRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomETRegular(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "HindVadodara-Regular.ttf");
        setTypeface(typeface, 1);

    }

}