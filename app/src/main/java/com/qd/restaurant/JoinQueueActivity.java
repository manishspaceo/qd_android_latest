package com.qd.restaurant;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qd.R;
import com.qd.interfaces.ConstantInterface;
import com.qd.interfaces.OnTaskCompleteListener;
import com.qd.util.AppUtility;
import com.qd.util.ApplicationQd;
import com.qd.util.CustomAsyncTask;
import com.qd.util.QdPreferences;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class JoinQueueActivity extends Activity implements View.OnClickListener, ConstantInterface {

    private LinearLayout llnotify;
    private TextView textBack;
    private TextView textName;
    private TextView textEstTime;
    private TextView textQueue;
    private ImageView imgResto;
    private AppUtility appUtility;
    private QdPreferences qdPreferences;
    private int restaurantsId;
    private int slowMode;
    private TextView textLeave;
    private ImageView imgNotifyMe;
    private Handler handler;
    private Runnable runnable;
    private int counter = 0;
    private int waitTime;

    // Default android methods start
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_resto);
        initControls();

        Bundle bundle = getIntent().getExtras();//
        if (bundle != null) {

            if (bundle.containsKey("isSLowModeOn")) {
                if (bundle.getBoolean("isSLowModeOn")) {
                    imgNotifyMe.setVisibility(View.GONE);
                    slowMode = 0; // Passing opposite value. So that it can be used as a toggle value.
                } else {
                    imgNotifyMe.setVisibility(View.VISIBLE);
                    slowMode = 1; // Passing opposite value. So that it can be used as a toggle value.
                }

            }
            if (bundle.containsKey("id")) {
                restaurantsId = bundle.getInt("id");
                if (appUtility.isInternetEnable(this)) {
                    appUtility.showProgressDialog(this, getResources().getString(R.string.action_loading));
                    joinQueue();
                }
            }
        }

    }

    // Initializing controls
    private void initControls() {

        appUtility = ApplicationQd.getInstance().getAppUtility();
        qdPreferences = ApplicationQd.getInstance().getSharedPreferences();
        llnotify = (LinearLayout) findViewById(R.id.llNotify);
        llnotify.setOnClickListener(this);
        textBack = (TextView) findViewById(R.id.textBack);
        textBack.setOnClickListener(this);
        textName = (TextView) findViewById(R.id.textName);
        textEstTime = (TextView) findViewById(R.id.textEstTime);
        textQueue = (TextView) findViewById(R.id.textQueue);
        imgResto = (ImageView) findViewById(R.id.imgResto);
        textLeave = (TextView) findViewById(R.id.textLeave);
        textLeave.setOnClickListener(this);
        imgNotifyMe = (ImageView) findViewById(R.id.imgNotifyMe);
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {

                if (counter < 10) {
                    counter++;
                    textEstTime.setText(--waitTime + " minutes");
                    if (waitTime != 0) {
                        handler.postDelayed(runnable, 60000);
                    }
                } else {
                    counter = 0;
                    if (appUtility.isInternetEnable(JoinQueueActivity.this)) {
                        joinQueue();
                    }
                }

            }
        };

    }

    // setting page values
    private void setValues(String resultString) {

        try {

            JSONObject jsonObject = new JSONObject(resultString).getJSONObject("queue_details");

            textName.setText(jsonObject.getString("name"));
            waitTime = Integer.parseInt(jsonObject.getString("estimate_wait_time"));
            textEstTime.setText(waitTime + " minutes");
            textQueue.setText(jsonObject.getString("in_the_queue"));

            Picasso.with(JoinQueueActivity.this).load(jsonObject.getString("image")).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(imgResto);

            if (waitTime != 0) {
                handler.postDelayed(runnable, 60000);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    // Api call to join queue.
    private void joinQueue() {

        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("joinqueue[user_id]", "" + qdPreferences.getPreference(USER_ID, -1));
        hashMap.put("joinqueue[restaurant_id]", "" + restaurantsId);


        new CustomAsyncTask(JOIN_QUEUE, hashMap, null, new OnTaskCompleteListener() {
            @Override
            public void onComplete(String resultString) {

                appUtility.showLog("testAPI response >> " + resultString);
                appUtility.hideProgressDialog();

                if (!resultString.equals("Error")) {
                    try {

                        JSONObject jsonObject = new JSONObject(resultString);
                        if (jsonObject.getInt("code") == 200) {

                            setValues(resultString);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    appUtility.showToast(getResources().getString(R.string.json_exception), Toast.LENGTH_SHORT);
                }

            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    // Api call to join queue.
    private void leaveQueue() {
        appUtility.showProgressDialog(this, getResources().getString(R.string.action_loading));

        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("user_id", "" + qdPreferences.getPreference(USER_ID, -1));
        hashMap.put("restaurant_id", "" + restaurantsId);


        new CustomAsyncTask(LEAVE_QUEUE, hashMap, null, new OnTaskCompleteListener() {
            @Override
            public void onComplete(String resultString) {

                appUtility.showLog("testAPI response >> " + resultString);
                appUtility.hideProgressDialog();

                if (!resultString.equals("Error")) {
                    try {

                        JSONObject jsonObject = new JSONObject(resultString);
                        if (jsonObject.getInt("code") == 200) {

                            setResult(RESULT_OK);
                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    appUtility.showToast(getResources().getString(R.string.json_exception), Toast.LENGTH_SHORT);
                }

            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    // Ask user to leave queue.
    private void askToLeave() {

        final AlertDialog.Builder alBuilder = new AlertDialog.Builder(this);
        alBuilder.setTitle(R.string.app_name);
        alBuilder.setMessage(R.string.sure_to_leave);
        alBuilder.setPositiveButton(R.string.leave, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (appUtility.isInternetEnable(JoinQueueActivity.this)) {
                    leaveQueue();
                }
            }
        });
        alBuilder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alBuilder.show();

    }

    // Api call to set Slow mode.
    private void setSlowMode() {

        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("userslowtimealert[restaurant_id]", "" + restaurantsId);
        hashMap.put("userslowtimealert[user_id]", "" + qdPreferences.getPreference(USER_ID, -1));
        hashMap.put("userslowtimealert[is_show_time_alert]", "" + slowMode);

        new CustomAsyncTask(UPDATE_SLOW_TIME_ALERT, hashMap, null, new OnTaskCompleteListener() {
            @Override
            public void onComplete(String resultString) {
                appUtility.showLog("testAPI response >> " + resultString);

                if (!resultString.equals("Error")) {
                    try {

                        JSONObject jsonObject = new JSONObject(resultString);
                        appUtility.hideProgressDialog();
                        if (jsonObject.getInt("code") == 200) {

                            Intent personal = new Intent("slowMode");
                            LocalBroadcastManager.getInstance(JoinQueueActivity.this).sendBroadcast(personal);
                            setResult(RESULT_CANCELED);
                            finish();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    // All the click listeners
    @Override
    public void onClick(View view) {

        handler.removeCallbacks(runnable);

        switch (view.getId()) {

            case R.id.textBack:

                setResult(RESULT_CANCELED);
                finish();
                break;

            case R.id.llNotify:

                if (appUtility.isInternetEnable(this)) {
                    appUtility.showProgressDialog(this, getString(R.string.action_loading));
                    setSlowMode();
                }
                break;

            case R.id.textLeave:

                askToLeave();
                break;

            default:
                break;

        }

    }

}
